
# Chapitre : programmation
## Partie 1
Projection de la fiche.
répartissez en binôme et donner des ordres au robot pour qu'il arrive à sa base spatiale.
on savant que le robot ne comprend que des instructions précises comme « va
2 fois tout droit, tourne à gauche » et non pas « va à ta base ». 

## Partie 2
Après 30 minutes de recherche, un binôme propose son travail au tableau.
On execute le programme instruction par instruction au tableau pour le valider ensemble.
Après plusieurs participations les élèves vont
remarquer que plusieurs chemins sont possibles et que chaque binôme a sûrement
utilisé des instructions différentes.
Essayer de développer le lexique sur la programmation.
Une suite d’instructions est un algorithme. 
La liste de tous les symboles est appelée un
langage de programmation. Traduire l’algorithme dans ce langage permet d’écrire un
programme.
Conclusion : on code les déplacements par des flèches vers le haut, le bas, la droite, la gauche.
D’autres situations peuvent être représentées en plaçant simplement le point de départ du
robot et la base à différents endroits sur le quadrillage.

## Partie 3
Les élèves synthétisent collectivement ce qui ont été appris au cours de cette séance.
On dessinera par exemple la grille de déplacement avec un ordinateur sur le cahier, son point de départ et
d’arrivée(la prise), et on écrira les instructions de déplacement.

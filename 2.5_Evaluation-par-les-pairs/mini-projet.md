##Mini Projet:
**Module :** Programmation

un robot a visité votre collège et vous avez une heure avec ce robot pour vous aider 
aà accomplir une tache de votre choix, décrire la tâche, le résultat souhaité et 
citez les instructions que vous avez donné au robot.
grille de l'évaluation:

-------------------------------------------------------------
Critères		        |Mauvais|Moyen	|Bien	|Très Bien
-------------------------------------------------------------
Idée de l'activité  	|	    |	    |	    |
-------------------------------------------------------------	
Organisation du texte	|	    |	    |	    |
-------------------------------------------------------------
Ordre des instructions	|	    |	    |	    |
-------------------------------------------------------------
Résultat	           	|   	|   	|	    |
-------------------------------------------------------------
Présentation	       	|	    |	    |	    |
-------------------------------------------------------------

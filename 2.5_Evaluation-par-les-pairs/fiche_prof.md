**Thématique :** Programmation

**Notions liées :** Instructions

**Résumé de l'activité :** Activité débranchée en 4 parties.Déplacement d'un robot

**Objectifs :** 
* Comprendre la notion d’instruction
de programme et de code

* Approcher la programmation de déplacements
absolus ou logique allocentrée

**Auteur :** Wafae

**Durée de l’activité :** 2h

**Forme de participation :** en binôme

**Matériel nécessaire :** projecteur, tableau

**Préparation :** aucune

**Fiche élève activité :** Disponible [ici](https://gitlab.com/wafaeouharra/mooc-2-ressources/-/blob/main/2.5_Evaluation-par-les-pairs/fiche_eleve.md)


# Partie 1
## Sous-partie 1 : texte
une phrase sans rien

_Une phrase en italique_

**Une phrase en gras**

Un lien vers [fun-mooc.fr](http://www.fun-mooc.fr)

Une ligne de<sub>code</sub>

## Sous-partie 1 : listes

**Liste à puce**


* item

   * sous-item

   * sous-item

* item

* item

**Liste numérotée**

1. item

2. item

3. item

## Sous-partie 3 : code

```python
    def carre(n):
        return n**2

